INIT = 50
NLYAP = 100

w = 400.0
h = 400.0
x0 = 0
schema = 'abb' # Secuencia de iteración
lenSchema = len(schema)

'''
La función a iterar
'''
def f(x, r):
    return r*x*(1 - x)

'''
La derivada de la función a iterar
'''
def der_f(x, r):
    return r - 2*r*x

'''
Inicialización de x
'''
def init(func, x0, r1, r2):
    x = x0
    j = 1
    for i in range(INIT):
        r = r1 if schema[j-1] == 'a' else r2
        j = j+1 if j % lenSchema != 0 else 1
        x = func(x, r)
    return x

'''
Cálculo del exponente de lyapunov
'''
def exponent(df, x, r1, r2):
    total = 0
    j = 1
    for i in range(NLYAP):
        r = r1 if schema[j-1] == 'a' else r2
        j = j+1 if j % lenSchema != 0 else 1
        x = r*x*(1-x)
        total = total + log(abs(df(x,r)))/log(2)
    expo = total/NLYAP
    return expo

'''
Función sigmoide para transformar el intervalo -inf, inf en 0, 255
'''
def sigmoid(x):
    return floor(255/(1 + 0.15*exp(-x)))
                   
''' función para convertir una coordenada 2D en 1D'''
def c(a,b):
    if a >= w or a < 0:
        pass
    if b >= w or b < 0:
        pass
    return int(w)*b + a

x0 = 0.21
r1 = sin(w)
r2 = cos(h)
x = init(f, x0, r1, r2)
l = exponent(der_f, x, r1, r2) 

size(400, 400) # No sé por que al pasar las variables w y h no funciona esta función.

img = createImage(int(w), int(h), RGB)
img.loadPixels()
for a in range(400):
    for b in range(400):
        _r1 = 3*a/w
        _r2 = 2*b/h
        r1 = _r1
        r2 = _r2
        x = 0.2#init(f, random(20)/20., r1, r2)
        l = exponent(der_f, random(20)/20., r1, r2)  
        #print(sigmoid(l)) 
        img.pixels[c(a,b)] = color(60, sigmoid(l), 60) 
   
#for i in range(len(img.pixels)):
#    img.pixels[i] = color(0, 90, 102)
img.updatePixels()
image(img, 0, 0)
